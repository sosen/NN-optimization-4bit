# DISFUNCTIONAL: The keras function is broken

import keras.models
from keras.utils.visualize_util import plot
import sys
sys.path.append("/afs/cern.ch/user/r/rixu/private/NN-optimization-4bit/pixel-NN-training")
import keras_utils


def main():
  model_file = open("/afs/cern.ch/work/r/rixu/ToT1-1000_ToT1-1000_100/ToT1-1000_ToT1-1000_100.model.yaml")
  model = keras.models.model_from_yaml(
      model_file.read(),
      custom_objects={'Sigmoid': keras_utils.Sigmoid}
      )
  plot(model, to_file='model.png')

  model_file.close()

if __name__ == '__main__':
  main()
