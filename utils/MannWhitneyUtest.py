"""
Modified from https://gitlab.cern.ch/sosen/MannWhitneyU

#Mann-Whitney U Test

Calculates the Mann-Whitney U Test statistic from two TH1F root histograms.
The program loops through the bins in h1 and calculates the rank for that bin
(i.e. the tied rank for every entry in the bin). U1 is then calculated using
the second method in the wiki. U values, normalized(?) U values, and the z
value are printed. The z value is returned.

The z value is the number of standard deviations from the mean. A high z value
indicates that h1 and h2 are significantly different in rank (i.e. values in h1
> values in h2, on average).
"""

from math import sqrt
import ROOT

def mannWU(h1, h2):
  n1 = h1.GetSize() # num bins + 2
  n2 = h2.GetSize()
  I1 = h1.Integral() + h1.GetBinContent(0) + h1.GetBinContent(n1-1) #adding the underflow and overflow bins by hand
  I2 = h2.Integral() + h2.GetBinContent(0) + h2.GetBinContent(n2-1)
  print("I1 = %d, I2 = %d" %(I1,I2))
  U1 = -0.5*I1*(I1 + 1)

  irank = 0 # number of entries in h1 < bin_i
  for i in range(n1):
    # ranking for every entry in bin h1_i
    rank = irank # add every entry below current bin
    rank += 0.5*(h1.GetBinContent(i) + 1) # add half from current bin too

    # add number of entries in h2 < bin_i in h1
    for j in range(n2):
      if (h1.GetBinCenter(i) > h2.GetBinCenter(j)):
        rank = rank + h2.GetBinContent(j)
      elif (h1.GetBinCenter(i) == h2.GetBinCenter(j)):
        rank = rank + 0.5*h2.GetBinContent(j)
        break

    U1 = U1 + rank*h1.GetBinContent(i)
    irank = irank + h1.GetBinContent(i)

  U2 = I1*I2 - U1

  # standardized value, from wiki page
  mU = I1*I2/2
  sU = sqrt(I1*I2*(I1+I2+1)/12) # use tie formula?
  z = abs(U1-mU)/sU

  print("U1 = %d, U2 = %d" %(U1,U2))
  print("U1/(I1*I2) = %f, U2/(I1*I2) = %f" %(U1/(I1*I2),U2/(I1*I2)))
  return z
