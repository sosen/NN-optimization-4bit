/* ROC_collect.cxx
 *
 * Collects graphs of the same name from several root files and combines them
 * in a single graph.
 *
 * Usage: ./ROC_collect outname graph_name [files ...]
 *
 * $udir/ROC_collect nToT_1M_1vs2 ROC_1vs2_all_all ToT5-1000000*\*.root
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <TFile.h>
#include <TGraph.h>
#include <TAxis.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TMultiGraph.h>

int main(int argc, char *argv[])
{
	if (argc < 4) {
		fprintf(stderr, "usage: %s outfile graph_name [files ...]\n", argv[0]);
		return 1;
	}

    /* Initialize TObjects */
    std::string out_name(argv[1]);
    std::string graph_name(argv[2]);
    TCanvas *c = new TCanvas("c","Canvas",1000,800);
    TMultiGraph *g = new TMultiGraph((graph_name + "_collection").c_str(),argv[2]);

    for (int i=3; i < argc; i++) {
        std::string file_name(argv[i]);
        size_t filestart = file_name.find("/") + 1;
        if (filestart != std::string::npos)
            file_name = argv[i] + filestart;

        TFile *file = new TFile(argv[i]);
        if (file == NULL) {
            fprintf(stderr, "Error: %s invalid file \n", argv[i]);
            exit(1);
        }

        TGraph *graph = (TGraph *)file->Get(argv[2]);
        if (graph == NULL) {
            fprintf(stderr, "Error: %s invalid graph name \n", argv[2]);
            exit(1);
        }

        TGraph *copy_graph = (TGraph *)graph->Clone();
        copy_graph->SetName((graph_name + "-" + file_name).c_str());
        copy_graph->SetTitle(file_name.c_str());
        copy_graph->SetMarkerStyle(17+i);
        copy_graph->SetMarkerColor(i);
        copy_graph->SetMarkerSize(.1);
        copy_graph->SetLineColor(i-2);
        copy_graph->SetLineWidth(2);
        copy_graph->SetFillStyle(0);
        copy_graph->SetFillColor(0);

        g->Add(copy_graph);

        file->Close();
    }

    c->SetLogx();
    g->Draw("AL");
    g->GetYaxis()->SetTitle("False Negative Rate");
    g->GetXaxis()->SetTitle("False Positive Rate");
    TLegend *l = c->BuildLegend(0.9,0.9,0.3,0.7); // x_tr, y_tr, x_bl, y_bl
    l->SetTextSize(.02);
    c->SaveAs((out_name + ".png").c_str());
    g->SaveAs((out_name + ".root").c_str());

    g->Delete();
    c->Close();

	return 0;
}


