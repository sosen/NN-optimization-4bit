#!/bin/bash
#
# launch_hop_number.bash : script for running the number neural network training
# and testing protocols for Hyperparameter OPtimization. A summary of the neural
# network efficiencies, consisting of the normalized U-test parameters, can be
# found in ...
#
# screen
# ./launch_hop_number.bash > out.txt

function usage()
{
    echo "usage: $0 #"
}

pdir='/afs/cern.ch/user/r/rixu/private/NN-optimization-4bit/pixel-number-crossval'
wdir='/afs/cern.ch/work/r/rixu'

############         MAIN         ###############

case $1 in
    0)
        setname='ToT5_1M.ToT1_100K.60.s64.x6'
        mkdir -p $wdir/$setname
        cd $wdir/$setname
        if [ $? -ne 0 ]; then exit 63; fi

        $pdir/launch.bash \
            --train $wdir/data/pToT5_1M-cut60.root \
            --test $wdir/data/pToT1_100K-cut60.root \
            --out $setname \
            --type number \
            --numpy-seed 64 \
            --validation-num 6 \
        ;;
    1)
        setname='ToT5_1M.ToT1_100K.60.s101.x6'
        mkdir -p $wdir/$setname
        cd $wdir/$setname
        if [ $? -ne 0 ]; then exit 63; fi

        $pdir/launch.bash \
            --train $wdir/data/pToT5_1M-cut60.root \
            --test $wdir/data/pToT1_100K-cut60.root \
            --out $setname \
            --type number \
            --numpy-seed 101 \
            --validation-num 6 \
        ;;
    2)
        setname='ToT5_1M.ToT1_100K.60.s122.x6'
        mkdir -p $wdir/$setname
        cd $wdir/$setname
        if [ $? -ne 0 ]; then exit 63; fi

        $pdir/launch.bash \
            --train $wdir/data/pToT5_1M-cut60.root \
            --test $wdir/data/pToT1_100K-cut60.root \
            --out $setname \
            --type number \
            --numpy-seed 122 \
            --validation-num 6 \
        ;;
    *)
        usage
        ;;
esac

