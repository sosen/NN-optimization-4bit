#!/bin/bash
#
# Script for launching the training and eval programs for the pixel NNs. For
# the training, the script will reload the python program every 20 epochs, to
# alleviate the memory leak in the training script. There is currently a
# hard-coded 1000 epoch cap.
#
# Example:
#
# source activate keras
# nohup bash $pdir/launch.bash -f $wdir/pToT5_10000-cut100.root -t $wdir/pToT1_1000-cut100.root
#       -o p100 -y number > p100.out.txt &
#
# Make sure to NOT setupATLAS yet


function usage()
{
    echo "usage: $0 <-option value>"
    echo "    -f, --train file              : training file path"
    echo "    -t, --test file               : testing file path"
    echo "    -o, --out name                : output header name"
    echo "    -y, --type type               : NN type {number,posN,errorNz}"
    echo ""
    echo "  ----    OPTIONAL    ----"
    echo ""
    echo "    -h, --help                    : print this message"
    echo "    -l, --learning-rate RATE      : learning rate (default = 0.08)"
    echo "    -m, --momentum MOMENTUM       : momentum (default = 0.4)"
    echo "    -r, --regularizer REGULARIZER : regularizer (default = 0.0000001)"
    echo "    -s, --lr-schedule NAME        : learning rate schedule name"
    echo "    -a, --momentum-schedule NAME  : momentum schedule name"
    echo "    -p, --patience PATIENCE       : patience (default = 20)"
    echo "    -N, --num-workers WORKERS     : number of parallel threads (default = 1)"
    echo "    --start-epoch EPOCH           : starting epoch for training (default = 0)"
    echo "    --max-epochs MAX_EPOCHS       : max number of epochs for training (default = 1000)"
    echo "    --numpy-seed SEED             : RNG seed for numpy"
}

unset NUMPY_SEED
unset PYTHONHASHSEED
################          Argument Parsing         #################
if [ $# -lt 8 ]; then usage; exit 1; fi
while [ -n "${1-}" ]; do
    case $1 in
        -f | --train )          shift
                                TRAINING=$1
                                ;;
        -t | --test )           shift
                                TEST=$1
                                ;;
        -o | --out )            shift
                                NAME=$1
                                ;;
        -y | --type )           shift
                                TYPE=$1
                                ;;
        -l | --learning-rate )  shift
                                LEARNING_RATE=$1
                                ;;
        -m | --momentum )       shift
                                MOMENTUM=$1
                                ;;
        -r | --regularizer )    shift
                                REGULARIZER=$1
                                ;;
        -s | --lr-schedule )    shift
                                LR_SCHEDULE=$1
                                ;;
        -a | --momentum-schedule )
                                shift
                                MOMENTUM_SCHEDULE=$1
                                ;;
        -N | --num-workers )    shift
                                NUM_WORKERS=$1
                                ;;
        -p | --patience )       shift
                                PATIENCE=$1
                                ;;
        --start-epoch )         shift
                                EPOCH=$1
                                ;;
        --max-epochs )          shift
                                MAX_EPOCHS=$1
                                ;;
        --numpy-seed )          shift
                                export NUMPY_SEED=$1
                                export PYTHONHASHSEED=0
                                ;;
        --train-only )          TRAIN_ONLY=1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

if [ -z ${LEARNING_RATE+x} ]; then
    LEARNING_RATE=0.08
fi
if [ -z ${MOMENTUM+x} ]; then
    MOMENTUM=0.4
fi
if [ -z ${REGULARIZER+x} ]; then
    REGULARIZER=0.0000001
fi
if [ -z ${LR_SCHEDULE+x} ]; then
    LR_SCHEDULE=0
fi
if [ -z ${MOMENTUM_SCHEDULE+x} ]; then
    MOMENTUM_SCHEDULE=0
fi
if [ -z ${NUM_WORKERS+x} ]; then
    NUM_WORKERS=1
fi
if [ -z ${PATIENCE+x} ]; then
    PATIENCE=20
fi
if [ -z ${EPOCH+x} ]; then
    EPOCH=0
fi
if [ -z ${MAX_EPOCHS+x} ]; then
    MAX_EPOCHS=1000
fi
if [ -z ${TRAIN_ONLY+x} ]; then
    TRAIN_ONLY=0
fi

SIZEX=7
SIZEY=7


#################           MAIN           #################
source /afs/cern.ch/user/r/rixu/anaconda2/bin/activate keras
pixelDir='/afs/cern.ch/user/r/rixu/private/NN-optimization-4bit/pixel-NN-training'
currDir=$(pwd)

# treat unset variables as error
set -u
# exit on failed pipe
#set -e
# set exit status of a pipe to last non-zero value
set -o pipefail


echo "       BEGINNING TRAINING        " >&2

if [ $TYPE = "number" ]; then
    while [ $EPOCH -lt $MAX_EPOCHS ]; do
        python $pixelDir/trainNN_keras.py \
            --training-input $TRAINING \
            --output $NAME \
            --config <(python $pixelDir/genconfig.py --type $TYPE --sizeX $SIZEX --sizeY $SIZEY) \
            --structure 25 20 \
            --output-activation softmax \
            --l2 $REGULARIZER \
            --learning-rate $LEARNING_RATE \
            --momentum $MOMENTUM \
            --batch 60 \
            --max-epochs 20 \
            --nbworkers $NUM_WORKERS \
            --patience $PATIENCE \
            --verbose \
            --profile \
            --weights "$currDir/$NAME.weights.hdf5" \
            --current-epoch $EPOCH \
            --lr-schedule $LR_SCHEDULE \
            --momentum-schedule $MOMENTUM_SCHEDULE \
            | tee -a $NAME.out.txt
        if [ $? -eq 1 ]; then break; fi
        EPOCH=$(( $EPOCH + 20 ))
        echo "Finished $EPOCH/$MAX_EPOCHS epochs in training"
    done

elif [ $(python -c "print $TYPE.startswith('pos')") == "True" ]; then
    python $pixelDir/trainNN_keras.py \
        --training-input $TRAINING \
        --output $NAME \
        --config <(python $pixelDir/genconfig.py --type $TYPE --sizeX $SIZEX --sizeY $SIZEY) \
        --structure 40 20 \
        --output-activation linear \
        --l2 0.0000001 \
        --learning-rate 0.04 \
        --momentum 0.3 \
        --batch 30 \
        --verbose

else
    python $pixelDir/traiNN_keras.py \
	    --training-input $TRAINING \
	    --output $NAME \
	    --config <(python $pixelDir/genconfig.py --type $TYPE --sizeX $SIZEX --sizeY $SIZEY) \
	    --structure 25 20 \
	    --output-activation softmax \
	    --l2 0.000001 \
	    --learning-rate 0.3 \
	    --momentum 0.7 \
	    --batch 30 \
	    --verbose
fi

if [ $TRAIN_ONLY -eq 1 ]; then exit 0; fi

echo "        BEGINNING EVALUATION       " >&2

python $pixelDir/evalNN_keras.py \
    --input $TEST \
    --model $NAME.model.yaml \
    --weights $NAME.weights.hdf5 \
    --config <(python $pixelDir/genconfig.py --type $TYPE --sizeX $SIZEX --sizeY $SIZEY) \
    --output $NAME.db \
    --normalization $NAME.normalization.txt

python $pixelDir/plotLosses.py --training-loss $NAME.training_loss.txt \
    --validation-loss $NAME.validation_loss.txt --out $NAME.loss_graph.png

echo "        BEGINNING TESTING       " >&2
set +e
set +u

. /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
. /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/packageSetups/localSetup.sh root
$pixelDir/test-driver.bash $TYPE $NAME.db $NAME.root $SIZEY
