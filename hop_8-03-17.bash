#!/bin/bash
#
# hop_mm-dd-yy.bash : script for running the number neural network training
# and testing protocols for Hyperparameter OPtimization.
#
# This file takes in a single integer argument >= 0, for ease in submitting
# using HTCondor. The individual submissions are hard-coded, and should be
# edited accordingly.
#
# ./hop_mm-dd-yy.bash 0 > out.txt

function usage()
{
    echo "usage: $0 #"
}

# THESE SHOULD BE EDITED APPROPRIATELY.
# CHANGING pdir from pixel-NN-training to pixel-number-crossval switches to
# cross-validation
pdir='/afs/cern.ch/user/r/rixu/private/NN-optimization-4bit/pixel-NN-training'
wdir='/afs/cern.ch/work/r/rixu'

############         MAIN         ###############

case $1 in
    0)
        setname='ToT5_1M.ToT1_100K.s64.n10'
        mkdir -p $wdir/$setname
        cd $wdir/$setname
        if [ $? -ne 0 ]; then exit 63; fi

        $pdir/launch.bash \
            --train $wdir/data/pToT5_1M.root \
            --test $wdir/data/pToT1_100K.root \
            --out $setname \
            --type number \
            --num-workers 10 \
            --numpy-seed 64 \
        ;;
    1)
        setname='ToT5_1M.ToT1_100K.s64.n10.t2'
        mkdir -p $wdir/$setname
        cd $wdir/$setname
        if [ $? -ne 0 ]; then exit 63; fi

        $pdir/launch.bash \
            --train $wdir/data/pToT5_1M.root \
            --test $wdir/data/pToT1_100K.root \
            --out $setname \
            --type number \
            --num-workers 10 \
            --numpy-seed 64 \
        ;;
    2)
        setname='ToT5_1M.ToT1_100K.s101.n10'
        mkdir -p $wdir/$setname
        cd $wdir/$setname
        if [ $? -ne 0 ]; then exit 63; fi

        $pdir/launch.bash \
            --train $wdir/data/pToT5_1M.root \
            --test $wdir/data/pToT1_100K.root \
            --out $setname \
            --type number \
            --num-workers 10 \
            --numpy-seed 101 \
        ;;
    3)
        setname='ToT5_1M.ToT1_100K.s122.n10'
        mkdir -p $wdir/$setname
        cd $wdir/$setname
        if [ $? -ne 0 ]; then exit 63; fi

        $pdir/launch.bash \
            --train $wdir/data/pToT5_1M.root \
            --test $wdir/data/pToT1_100K.root \
            --out $setname \
            --type number \
            --num-workers 10 \
            --numpy-seed 122 \
        ;;
    4)
        setname='ToT5_1M.ToT1_100K.s150.n10'
        mkdir -p $wdir/$setname
        cd $wdir/$setname
        if [ $? -ne 0 ]; then exit 63; fi

        $pdir/launch.bash \
            --train $wdir/data/pToT5_1M.root \
            --test $wdir/data/pToT1_100K.root \
            --out $setname \
            --type number \
            --num-workers 10 \
            --numpy-seed 150 \
        ;;
    5)
        setname='ToT5_4M.ToT1_100K.s150.n10'
        mkdir -p $wdir/$setname
        cd $wdir/$setname
        if [ $? -ne 0 ]; then exit 63; fi

        $pdir/launch.bash \
            --train $wdir/data/pToT5_4M.root \
            --test $wdir/data/pToT1_100K.root \
            --out $setname \
            --type number \
            --num-workers 10 \
            --numpy-seed 150 \
        ;;
    *)
        usage
        ;;
esac

