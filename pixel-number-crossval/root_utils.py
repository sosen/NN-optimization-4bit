import thread
import threading

import numpy as np
import ROOT
import root_numpy as rnp


def iround(x, base):
    return int(base * int(float(x)/base))

def calc_normalization(path, tree, branches):
    '''
    path = path to ROOT input file
    tree = tree name in file
    branches = names of branches in tree -- inputs to NN!

    Returns the standard deviation and mean of each branch
        (each entry in the input matrix)
    '''
    tfile = ROOT.TFile(path, 'READ')
    tree = tfile.Get(tree)
    mean = np.zeros(len(branches))
    std = np.ones(len(branches))
    for i, branch in enumerate(branches):
        arr = rnp.tree2array(tree, branches=branch)
        mean[i] = np.mean(arr)
        std[i] = np.std(arr)
        if std[i] == 0:
            std[i] = 1
    return {'std': std, 'mean': mean}


# http://stackoverflow.com/questions/5957380/convert-structured-array-to-regular-numpy-array
def root_batch(tree, branches, bounds, normalization):
    '''
    Converts elements within bounds of the target tree to a numpy array.
    Shifts and normalizes the data.
    '''
    batch = rnp.tree2array(
        tree=tree,
        branches=branches,
        start=bounds[0],
        stop=bounds[1]
    )
    batch = batch.view(np.float64).reshape(batch.shape + (-1,))
    batch -= normalization['mean']
    batch *= (1.0/normalization['std'])
    return batch


# https://github.com/fchollet/keras/issues/1638#issuecomment-179744902
class ThreadsafeIter(object):
    """Takes an iterator/generator and makes it thread-safe by
    serializing call to the `next` method of given iterator/generator.
    """
    # pylint: disable=too-few-public-methods
    def __init__(self, itr):
        self.itr = itr
        self.lock = threading.Lock()

    def __iter__(self):
        return self

    def next(self):
        with self.lock:
            return self.itr.next()


def threadsafe_generator(gen):
    """A decorator that takes a generator function and makes it thread-safe.
    """
    def safegen(*a, **kw):
        return ThreadsafeIter(gen(*a, **kw))
    return safegen


@threadsafe_generator
def generator(path,
              tree,
              branches,
              validation_i=0,
              validation_num=4,
              batch=32,
              normalization=None,
              loop=True): \
              # pylint: disable=too-many-arguments
    '''
    Generates the data from specified tree, with each iteration returning a
    normalized numpy array containing batch number of events. Event data are
    normalized and shifted.

    path -- to file; tree -- tree name; branches -- config data
    validaiton_i -- index of validation set
    validaiton_num -- number of cross-validations
    batch -- minibatch size
    normalizaiton -- {'std': std, 'mean': mean} arrays for the 49 pixels
    loop -- True
    '''

    tfiles = {}
    trees = {}

    nentries = get_entries(path, tree)
    cv_size = int(nentries / validation_num)

    if normalization is None:
        normalization = {'std': 1, 'mean': 0}

    data_range = range(0, validation_i*cv_size, batch) \
                + range(iround((validation_i+1)*cv_size,cv_size), nentries, batch)
    if loop==False:
        data_range = range(0, nentries, batch)
    else:
        data_range = range(0, validation_i*cv_size, batch) \
                    + range(iround((validation_i+1)*cv_size,cv_size), nentries, batch)

    while True:
        for i in data_range:
            thr = thread.get_ident()

            if thr not in trees:
                tfiles[thr] = ROOT.TFile(path, 'READ')
                trees[thr] = tfiles[thr].Get(tree)

            xbatch = root_batch(
                tree=trees[thr],
                branches=branches[0],
                bounds=(i, i+batch),
                normalization=normalization
            )

            if len(branches[1]) > 0:
                ybatch = root_batch(
                    tree=trees[thr],
                    branches=branches[1],
                    bounds=(i, i+batch),
                    normalization={'mean': 0, 'std': 1} # no normalization
                )
            else:
                ybatch = None

            yield (xbatch, ybatch)


        if not loop:
            break


def load_validation(path, tree, branches, normalization, validation_i,
        validation_num):
    '''
    Loads the entirety of the validation data into memory
    '''

    tfile = ROOT.TFile(path, 'READ')
    tree = tfile.Get(tree)

    if normalization is None:
        normalization = {'std': 1, 'mean': 0}

    nentries = tree.GetEntries()
    cv_size = int(nentries / validation_num)
    start = validation_i * cv_size
    end = (validation_i + 1) * cv_size

    xdat = root_batch(
        tree=tree,
        branches=branches[0],
        bounds=(start, end),
        normalization=normalization,
    )
    ydat = root_batch(
        tree=tree,
        branches=branches[1],
        bounds=(start, end),
        normalization={'mean': 0, 'std': 1}
    )

    tfile.Close()
    return xdat, ydat


def get_entries(path, tree):
    tfile = ROOT.TFile(path, 'READ')
    ttree = tfile.Get(tree)
    n = ttree.GetEntries()
    tfile.Close()
    return n
