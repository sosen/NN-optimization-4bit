/* ROC_MWU.cxx
 *
 * A program that takes (+ truth, + predicted) probabilites, output from a
 * SQL query, and calculates the Mann-Whitney U factor and the corresponding
 * noramlized statistic z.
 *
 * See the wiki for the calculations (method 2).
 *
 * "+truth +pred" pairs taken from stdin, delimited by spaces and newlines.
 * Outputs to stdout.
 *
 * Smaller U1 values are better, larger U2 values are better, larger z values
 * are better.
 *
 * usage: ./ROC_MWU name [-v]
 */


#include <float.h>
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
	signal(SIGPIPE, SIG_DFL); // default

    bool verbose = false;
    if (argc == 3) verbose = true;
    else if (argc != 2) {
		fprintf(stderr, "usage: %s name [-v] \n", argv[0]);
		return 1;
	}

    double n1 = 0;
    double n2 = 0;
    double rank = 1;
    double U1 = 0;

	size_t len = 2048;
	char *lineptr = (char*)malloc(sizeof(char) * len);

	while (getline(&lineptr, &len, stdin) > -1) {
		double key; // 1 or 0 -> positive or negative
		double p;   // predicted positive probability

		if (sscanf(lineptr, "%lf %lf\n", &key, &p) != 2) {
			fprintf(stderr, "ERROR: %s: malformed line: %s\n", argv[0], lineptr);
			return 1;
		}

        if (key == 1) {
            U1 += rank;
            n1++;
        }
        else
            n2++;
        rank++;
	}

    U1 = U1 - (n1 * (n1+1))/2;
    double U2 = n1*n2 - U1;
    double mU = n1*n2/2;
    double sU = sqrt(n1*n2*(n1+n2+1)/12);
    double z  = fabs(U1 - mU) / sU;

    if (verbose) {
        fprintf(stderr,"U1 = %lf; U2 = %lf\n",U1,U2);
        fprintf(stderr,"n1 = %lf; n2 = %lf; rank = %lf\n",n1,n2,rank);
        fprintf(stderr,"mU = %lf; sU = %lf\n",mU,sU);
    }
    printf("%s %lf %lf\n", argv[1], z, U1/(n1*n2));

    free(lineptr);

	return 0;
}
