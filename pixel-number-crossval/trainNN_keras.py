"""
trainNN_keras: module to train a neural network

Arguments: See arguments of train_nn and main. Set environment variable
NUMPY_SEED to set the numpy random generator seed.

This function creates 5 output files:
    output.normalization.txt -- An array of means and standard deviations
                                of the input nodes; from utils.py
    output.model.yaml -- A Keras file detailing the NN model architecture
    output.training_loss.txt -- A list of training losses per epoch
    output.validation_loss.txt -- A list of validation losses per epoch
    output.weights.hdf5 -- Best fit weights, created by keras callbacks
    output.callback_config.txt -- Config file for the checkpoint callback
    ----------------------------------
    IF save_all
        output.{epoch:03d}.hd5 -- weight files for each epoch
    IF profile
        output.profile.txt -- CPU and time usage of each epoch

A memory leak exists from an unknown source, but most likely something
intrinsic to keras and theano.

Alternative hack: Use a shell script to reload the program every 20 epochs.

This program implements k-fold cross-validation, with the k-value set with
argument validation-num. The function train_nn only trains ONE of the k
NNs, specified by the argument validation_i.
"""

import os
import numpy as np
if os.environ.has_key("NUMPY_SEED"):
    print("Setting numpy seed: " +  os.environ["NUMPY_SEED"])
    np.random.seed(int(os.environ["NUMPY_SEED"]))
else:
    print("Note: no numpy seed specified")
import sys
import argparse

from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras.layers.core import Dense, Activation
from keras.models import Sequential, load_model
from keras.optimizers import SGD
import keras.regularizers


# user files
from keras_utils import Sigmoid, Profile, ThresholdEarlyStopping
import keras_utils
import root_utils
import utils
import keras_schedules


__all__ = ['train_nn']


def train_nn(training_input,
             output,
             config,
             validation_i=0,
             validation_num=4,
             structure=[25, 20],
             activation='sigmoid',
             output_activation='softmax',
             regularizer=0.0000001,
             learning_rate=0.08,
             momentum=0.4,
             batch=50,
             max_epochs=20,
             patience=5,
             profile=False,
             nbworkers=1,
             save_all=False,
             verbose=False,
             curr_epoch=0,
             weights=None,
             lr_schedule=None,
             momentum_schedule=None
             ): \
            # pylint: disable=too-many-arguments,too-many-locals,dangerous-default-value
    """ train a neural network

    Arguments:
        training_input -- path to ROOT training dataset
        output -- prefix for output files
        validation_i -- index of validation set (out of [0,validation_num) )
        validation_num -- number of folds for cross-validation
        structure -- list with number of hidden nodes in hidden layers
        activation -- non-linearity for hidden layers
        output_activation -- non-linearity of output of the network
        regularizer -- l2 weight regularizer
        momentum -- momentum of weight updates
        batch -- minibatch size
        max_epochs -- maximum number of training epochs
        patience -- early stopping tolerance, in number of epochs
        profile -- create a memory usage log
        nbworkers -- number of parallel thread to load minibatches
        save_all -- save weights at each epoch
        verbose -- bla bla bla level
        curr_epoch -- current epoch number, for discontinuous jobs
        weights -- load an initial set of weights (curr_epoch > 0)
        lr_schdule -- use a preset learning rate scheduler
    """

    """                         DATA                          """

    branches = utils.get_data_config_names(config, meta=False)
    # branches is a 2d array of names filled from the config file
    #   branches[0] = inputs  : NN_matrix#, etc.
    #   branches[1] = targets : NN_nparticles#, for numberNN
    #   branches[2] = metadata, empty since meta = False
    norm = root_utils.calc_normalization(
        training_input,
        'NNinput',
        branches[0]
    )
    # norm is a dictionary {'std': std, 'mean': mean}, where std and mean
    # are 60-length nparrays containing the std/mean of each input
    utils.save_normalization(norm, output)

    data_generator = root_utils.generator(
        path=training_input,
        tree='NNinput',
        branches=branches,
        validation_i=validation_i,
        validation_num=validation_num,
        batch=batch,
        normalization=norm,
    )
    # a generator that returns (normalized data, targets) as numpy arrays in
    # batches of batch number of elements

    valid_data = root_utils.load_validation(
        path=training_input,
        tree='NNinput',
        branches=branches,
        normalization=norm,
        validation_i=validation_i,
        validation_num=validation_num
    )
    # (normalized validation data, targets) numpy arrays

    nentries = root_utils.get_entries(training_input, 'NNinput')


    """                           MODEL                             """

    from_file = False
    if curr_epoch > 0 and weights is not None:
        model = load_model(weights + ".tmp")
        from_file = True

    else:
        structure = [len(branches[0])] + structure + [len(branches[1])]
        model = _build_model(
            structure,
            regularizer,
            (activation, output_activation),
            learning_rate,
            momentum
        )
        with open(output + '.model.yaml', 'w') as yfile:
            yfile.write(model.to_yaml())


    """                         CALLBACKS                             """

    callbacks = [
        _checkpoint_callback(output, from_file, patience, verbose),
    ]
    # the checkpoint_callback is what saves the final weights

    if profile: # adds a new callback that saves the profiles each epoch
        callbacks.append(Profile('%s.profile.txt' % output, curr_epoch))

    if save_all: # adds a new callback that saves the model each epoch
        name = output
        if name.endswith('hdf5'):
            name = name.replace('hdf5', '{epoch:03d}.hdf5')
        else:
            name += '.{epoch:03d}.hdf5'
        callbacks.append(ModelCheckpoint(name))

    if lr_schedule and not lr_schedule == "0":
        callbacks.append(keras.callbacks.LearningRateScheduler(
            keras_schedules.lr_wrapper(lr_schedule,learning_rate,curr_epoch)
        ))

    if momentum_schedule and not momentum_schedule == "0":
        callbacks.append(keras_utils.MomentumScheduler(
            keras_schedules.momentum_wrapper(momentum_schedule,momentum,curr_epoch)
        ))


    """                           TRAIN                             """
    samples_per_epoch = int(batch * int(nentries * (1.-1./validation_num)/batch))

    history = model.fit_generator(
        generator=data_generator,
        samples_per_epoch=samples_per_epoch,
        nb_epoch=max_epochs,
        verbose=0,
        callbacks=callbacks,
        validation_data=valid_data,
        nb_worker=nbworkers,
        pickle_safe=False
    )

    with open(output+'.training_loss.txt', 'a') as tloss:
        np.savetxt(tloss, history.history['loss'])
    with open(output+'.validation_loss.txt', 'a') as vloss:
        np.savetxt(vloss, history.history['val_loss'])

    if len(history.history['val_loss']) < max_epochs:
        sys.exit(1) # notify shell script of early termination


def _build_model(structure,
                 regularizer,
                 activations,  # (inner layer activation, outer activation)
                 learning_rate,
                 momentum):
    '''Builds a Keras model with the supplied inputs. Layers are Dense.'''
    model = Sequential()
    for i in range(1, len(structure)):
        # by default, structure = [$numInputs,25,20,$numOutputs]
        model.add(
            Dense(
                input_dim=structure[i-1],
                output_dim=structure[i],
                init='glorot_uniform',
                W_regularizer=keras.regularizers.l2(regularizer)
            )
        )
        if i < (len(structure) - 1):
            if activations[0] == 'sigmoid2':
                model.add(Sigmoid(2))
            else:
                model.add(Activation(activations[0]))
        elif activations[1] == 'sigmoid2':
            model.add(Sigmoid(2))
        else:
            model.add(Activation(activations[1]))

    loss = 'categorical_crossentropy' if activations[1] != 'linear' else 'mae'
    model.compile(
        loss=loss,
        optimizer=SGD(lr=learning_rate, momentum=momentum)
    )
    return model


def _checkpoint_callback(filepath, from_file, patience, verbose):
    '''Creates a callback that saves the model after every epoch, and provides
    an early stopping functionality'''
    config_file = filepath + '.callback_config.txt'
    filepath += '.weights.hdf5'
    return keras_utils.Checkpoint(
        filepath=filepath,
        monitor='val_loss',
        save_best_only=True, #best save won't be overwritten
        mode='min',
        verbose=verbose,
        patience=patience,
        from_file=from_file,
        config_file=config_file
    )


def _main():

    parse = argparse.ArgumentParser()
    parse.add_argument('--training-input', required=True)
    parse.add_argument('--output', required=True)
    parse.add_argument('--config', required=True)
    parse.add_argument('--validation_i', type=int, default=0)
    parse.add_argument('--validation_num', type=int, default=4)
    parse.add_argument('--structure', nargs='+', type=int, default=[25, 20])
    parse.add_argument('--activation', choices=['sigmoid', 'sigmoid2', 'tanh', 'relu'], default='sigmoid')
    parse.add_argument('--output-activation', choices=['softmax', 'linear', 'sigmoid', 'sigmoid2'], default='softmax')
    parse.add_argument('--l2', type=float, default=0.0000001)
    parse.add_argument('--learning-rate', type=float, default=0.08)
    parse.add_argument('--momentum', type=float, default=0.4)
    parse.add_argument('--batch', type=int, default=50)
    parse.add_argument('--max-epochs', type=int, default=20)
    parse.add_argument('--patience', type=int, default=5)
    parse.add_argument('--profile', default=False, action='store_true')
    parse.add_argument('--nbworkers', default=1, type=int)
    parse.add_argument('--save-all', action='store_true', default=False)
    parse.add_argument('--verbose', default=False, action='store_true')
    parse.add_argument('--current-epoch', type=int, default=0)
    parse.add_argument('--weights',default=None)
    parse.add_argument('--lr-schedule',default=None)
    parse.add_argument('--momentum-schedule',default=None)
    args = parse.parse_args()

    train_nn(
        args.training_input,
        args.output + "_cv" + str(args.validation_i),
        args.config,
        args.validation_i,
        args.validation_num,
        args.structure,
        args.activation,
        args.output_activation,
        args.l2,
        args.learning_rate,
        args.momentum,
        args.batch,
        args.max_epochs,
        args.patience,
        args.profile,
        args.nbworkers,
        args.save_all,
        args.verbose,
        args.current_epoch,
        args.weights,
        args.lr_schedule,
        args.momentum_schedule
    )


if __name__ == '__main__':
    _main()
